# !/usr/bin/env bash
# author: therojam
# authoremail: github@therojam.tech
# Create PDF from LaTex file.
#

input=$1

while true
do
  case $input in
   [d]* ) pdflatex CV-DE.tex
	   mv CV-DE.pdf CV-$(date +%Y-%m).pdf
	   cp CV-$(date +%Y-%m).pdf ~/iCloud/CV-DE-$(date +%Y-%m).pdf
	   open ~/iCloud/CV-DE-$(date +%Y-%m).pdf
           exit 0 ;;


   [e]* ) pdflatex CV-EN.tex
	   mv CV-EN.pdf CV-$(date +%Y-%m).pdf
	   cp CV-$(date +%Y-%m).pdf ~/iCloud/CV-EN-$(date +%Y-%m).pdf
	   open ~/iCloud/CV-EN-$(date +%Y-%m).pdf
           exit 0 ;;
   
   * )     echo "## Dude, enter e\[nglish\] or d\[eutsch\] please. ##";
	   exit 1 ;;
  esac
done
